<?php
include 'inc/header.php';
$message = '';
$file_db = "server/php/data/db.lite";
if (isset($_POST['savenewuser'])) {
	if(isset($_POST['u1']) && $_POST['u1'] != '' && isset($_POST['u2']) && $_POST['u2'] != '' && isset($_POST['u3']) && $_POST['u3'] != ''){

		$conn = new PDO("sqlite:".$file_db) or die("1");
		$conn->exec("
			CREATE TABLE IF NOT EXISTS `user_login` (
			`id`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			`user`  text NOT NULL,
			`pass`  text NOT NULL,
			`email` text NOT NULL,
			`lang`  text,
			`pack`  text,
			`status`  INTEGER NOT NULL,
			`timestamp`     timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
			);
			");
		$stmt = $conn->prepare("INSERT INTO user_login (user,pass,email,lang,pack,status) VALUES (?,?,?,?,?,?)") or die("2");
		$stmt->execute(array($_POST['u1'],md5(md5($_POST['u3'])),$_POST['u2'],'thai','free','1')) or die("3");
		$conn = null;
		$message = '<h1 class="text-success text-center">สมัครสมาชิกเรียบร้อย</h1>';

	}else{
		$message = '<h1 class="text-danger text-center">คุณกรอกข้อมูลไม่ครบ</h1>';
	}
}

if(isset($_POST['logincheck'])){
	if(isset($_POST['u1']) && $_POST['u1'] != '' && isset($_POST['u2']) && $_POST['u2'] != ''){

		$conn = new PDO("sqlite:".$file_db) or die("1");
		$stmt = $conn->prepare("SELECT * FROM user_login WHERE email = '".$_POST['u1']."' AND pass = '".md5(md5($_POST['u2']))."' AND status = 1 LIMIT 1") or die("2");
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);

		$_SESSION['login'] = $stmt->fetch();
		// var_dump($_SESSION['login']);
		$conn = null;
		$message = '<h1 class="text-success text-center">เข้าสู่ระบบสำเร็จ</h1>';
		header("location: index.php");
	}else{
		$message = '<h1 class="text-danger text-center">คุณกรอกข้อมูลไม่ครบ</h1>';
	}
}

?>
<link href="css/signin.css" rel="stylesheet">
<div class="container">
	<br><br>
	<h2 class="form-signin-heading text-center">เข้าใช้งานระบบ</h2>

	<div class="card card-signin">
		<img class="img-circle profile-img" src="img/avatar.png" alt="">
		<form class="form-signin" action="" method="post">
			<label for="emailuser" class="sr-only">อีเมล</label>
			<input type="email" id="emailuser" class="form-control" name="u1" placeholder="อีเมล" required autofocus>
			<label for="passeduser" class="sr-only">รหัสผ่าน</label>
			<input type="password" id="passeduser" class="form-control" name="u2" placeholder="รหัสผ่าน" required>
			<button class="btn btn-lg btn-primary btn-block" type="submit" name="logincheck">เข้าระบบ</button>
		</form>
	</div>

	<p class="text-center">
		<a data-toggle="modal" data-target="#newuser" href="#">สร้างบัญชี</a>
	</p>

	<?php
	// var_dump($_SESSION['login']);
	if($message != ''){
		echo $message;
	}
	?>

	<!-- Modal -->
	<div class="modal fade" id="newuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">สร้างบัญชีใหม่</h4>
				</div>
				<form class="form-horizontal" action="" method="post">
					<div class="modal-body">

						<div class="form-group">
							<label for="input1" class="col-sm-2 control-label">ชื่อผู้ใช้</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="input1" name="u1" placeholder="ชื่อผู้ใช้" required>
							</div>
						</div>
						<div class="form-group">
							<label for="input2" class="col-sm-2 control-label">อีเมล</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="input2" name="u2" placeholder="อีเมล" required>
							</div>
						</div>
						<div class="form-group">
							<label for="input3" class="col-sm-2 control-label">รหัสผ่าน</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="input3" name="u3" placeholder="รหัสผ่าน" required>
							</div>
						</div>


					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">ปิดไป</button>
						<button class="btn btn-primary" type="submit" name="savenewuser">สร้างบัญชี</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<?php
include 'inc/footer.php';
?>